package app;

import org.testng.annotations.DataProvider;

/**
 * Created by gooornik on 11.09.2019.
 */
public class CommonApiDataProviders {

    @DataProvider
    public static Object[][] validGithubEndpoints(){
        return new Object[][]{
                {"user"},
                {"user/followers"},
                {"notifications"}
        };
    }

    @DataProvider
    public static Object[][] differentStatuses(){
        return new Object[][]{
                {"204"},
                {"401"},
                {"404"}
        };
    }


}
