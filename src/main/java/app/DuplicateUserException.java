package app;

/**
 * Created by gooornik on 11.09.2019.
 */
public class DuplicateUserException extends Exception {

    public DuplicateUserException(String message) {
        super(message);
    }
}
