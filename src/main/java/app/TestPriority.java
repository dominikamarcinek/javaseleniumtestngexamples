package app;

/**
 * Created by gooornik on 11.09.2019.
 */
public interface TestPriority {

    int HIGH = -10;

    int MEDIUM = 0;

    int LOW = 10;

}
