package performance

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import org.testng.annotations.Test

class BasicSimulation extends Simulation {

  val httpProtocol = http
    .baseUrl("http://5.196.7.235")
    .inferHtmlResources()
    .acceptHeader("image/webp,*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("pl,en-US;q=0.7,en;q=0.3")
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0")

  val headers_0 = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Upgrade-Insecure-Requests" -> "1")

  val headers_1 = Map("Accept" -> "text/css,*/*;q=0.1")

  val headers_2 = Map("Accept" -> "*/*")

  val headers_26 = Map(
    "Accept" -> "application/font-woff2;q=1.0,application/font-woff;q=0.9,*/*;q=0.8",
    "Accept-Encoding" -> "identity")

  val headers_45 = Map(
    "Accept" -> "application/json, text/javascript, */*; q=0.01",
    "Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
    "X-Requested-With" -> "XMLHttpRequest")

  val headers_46 = Map(
    "Accept" -> "*/*",
    "Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
    "X-Requested-With" -> "XMLHttpRequest")



  val scn = scenario("BasicSimulation")
    .exec(http("request_0")
      .get("/")
      .headers(headers_0))
    .pause(1)
    .exec(http("request_1")
      .get("/themes/classic/assets/css/custom.css")
      .headers(headers_1)
      .resources(http("request_2")
        .get("/themes/classic/assets/js/custom.js")
        .headers(headers_2),
        http("request_3")
          .get("/modules/ps_imageslider/css/homeslider.css")
          .headers(headers_1),
        http("request_4")
          .get("/modules/ps_imageslider/js/responsiveslides.min.js")
          .headers(headers_2),
        http("request_5")
          .get("/modules/ps_shoppingcart/ps_shoppingcart.js")
          .headers(headers_2),
        http("request_6")
          .get("/themes/classic/assets/css/theme.css")
          .headers(headers_1),
        http("request_7")
          .get("/modules/ps_searchbar/ps_searchbar.js")
          .headers(headers_2),
        http("request_8")
          .get("/themes/core.js")
          .headers(headers_2),
        http("request_9")
          .get("/themes/classic/assets/js/theme.js")
          .headers(headers_2),
        http("request_10")
          .get("/js/jquery/ui/jquery-ui.min.js")
          .headers(headers_2),
        http("request_11")
          .get("/modules/ps_imageslider/js/homeslider.js")
          .headers(headers_2),
        http("request_12")
          .get("/js/jquery/ui/themes/base/minified/jquery-ui.min.css")
          .headers(headers_1),
        http("request_13")
          .get("/js/jquery/ui/themes/base/minified/jquery.ui.theme.min.css")
          .headers(headers_1),
        http("request_14")
          .get("/5-home_default/today-is-a-good-day-framed-poster.jpg"),
        http("request_15")
          .get("/4-home_default/the-adventure-begins-framed-poster.jpg"),
        http("request_16")
          .get("/3-home_default/the-best-is-yet-to-come-framed-poster.jpg"),
        http("request_17")
          .get("/21-home_default/brown-bear-printed-sweater.jpg"),
        http("request_18")
          .get("/6-home_default/mug-the-best-is-yet-to-come.jpg"),
        http("request_19")
          .get("/7-home_default/mug-the-adventure-begins.jpg"),
        http("request_20")
          .get("/8-home_default/mug-today-is-a-good-day.jpg"),
        http("request_21")
          .get("/2-home_default/hummingbird-printed-t-shirt.jpg"),
        http("request_22")
          .get("/img/logo.png"),
        http("request_23")
          .get("/modules/ps_imageslider/images/sample-2.jpg"),
        http("request_24")
          .get("/modules/ps_imageslider/images/sample-1.jpg"),
        http("request_25")
          .get("/modules/ps_banner/img/sale70.png"),
        http("request_26")
          .get("/themes/classic/assets/css/570eb83859dc23dd0eec423a49e147fe.woff2")
          .headers(headers_26),
        http("request_27")
          .get("/themes/classic/assets/css/199038f07312bfc6f0aabd3ed6a2b64d.woff2")
          .headers(headers_26),
        http("request_28")
          .get("/themes/classic/assets/css/19c1b868764c0e4d15a45d3f61250488.woff2")
          .headers(headers_26),
        http("request_29")
          .get("/modules/ps_imageslider/images/sample-3.jpg"),
        http("request_30")
          .get("/img/favicon.ico?1324977642"),
        http("request_31")
          .get("/js/jquery/ui/themes/base/minified/images/ui-bg_flat_75_ffffff_40x100.png")))
    .pause(3)
    .exec(http("request_32")
      .get("/my-account")
      .headers(headers_0))
    .pause(13)
    .exec(http("request_33")
      .post("/login?back=my-account")
      .headers(headers_0)
      .formParam("back", "my-account")
      .formParam("email", "admin20@gmail.com")
      .formParam("password", "admin")
      .formParam("submitLogin", "1"))
    .pause(5)
    .exec(http("request_34")
      .post("/login?back=my-account")
      .headers(headers_0)
      .formParam("back", "my-account")
      .formParam("email", "admin20@gmail.com")
      .formParam("password", "admin20")
      .formParam("submitLogin", "1"))
    .pause(5)
    .exec(http("request_35")
      .get("/3-clothes")
      .headers(headers_0))
    .pause(4)
    .exec(http("request_36")
      .get("/women/2-9-brown-bear-printed-sweater.html")
      .headers(headers_0)
      .resources(http("request_37")
        .get("/themes/classic/assets/css/8b05d51ede908907d65695558974d86f.svg"),
        http("request_38")
          .get("/themes/classic/assets/css/082a71677e756fb75817e8f262a07cb4.svg"),
        http("request_39")
          .get("/themes/classic/assets/css/ffddcb3736980b23405b31142a324b62.svg"),
        http("request_40")
          .get("/themes/classic/assets/css/3a2aeeba930cc29e4d31ebfa1b7cdaa2.svg"),
        http("request_41")
          .get("/themes/classic/assets/css/e049aeb07a2ae1627933e8e58d3886d2.svg"),
        http("request_42")
          .get("/themes/classic/assets/css/c1a65805f759901a39d10eb854c1dcf2.svg"),
        http("request_43")
          .get("/themes/classic/assets/css/b1db819132e64a3e01911a1413c33acf.svg"),
        http("request_44")
          .get("/themes/classic/assets/css/99db8adec61e4fcf5586e1afa549b432.svg")))
    .pause(1)
    .exec(http("request_45")
      .post("/cart")
      .headers(headers_45)
      .formParam("token", "08c9a2b8b701e493458fbcc249ebb5d3")
      .formParam("id_product", "2")
      .formParam("id_customization", "0")
      .formParam("group[1]", "1")
      .formParam("qty", "1")
      .formParam("add", "1")
      .formParam("action", "update")
      .resources(http("request_46")
        .post("/module/ps_shoppingcart/ajax")
        .headers(headers_46)
        .formParam("id_product_attribute", "9")
        .formParam("id_product", "2")
        .formParam("action", "add-to-cart")))

  setUp(scn.inject(atOnceUsers(10))).protocols(httpProtocol)
}