package ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by gooornik on 11.09.2019.
 */

public class UiTestWithoutDataProvider {

    private WebDriver driver;

    @DataProvider
    public static Object[][] invalidLoginAndPassword () {
        return new Object[][]{
                {"let_me_in", ""},
                {"", "safest_password_in_the_world"}
        };
    }

    @BeforeMethod
    public void startUpBrowser(){
        driver = new ChromeDriver();
        driver.get("https://github.com/login");
    }

    @Test(dataProvider = "invalidLoginAndPassword")
    public void emptyLoginOrPasswordFailsLogin(String login, String password){
        //login
        driver.findElement(By.id("login_field")).sendKeys(login);
//        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.xpath("//input[@value='Sign in with your identity provider']")).click();
        // verify error appeared
        Assert.assertTrue(driver.findElement(By.className("flash-error"))
                .getText()
                .equalsIgnoreCase("Incorrect username or password."));
    }

    @AfterMethod
    public void teardown(){
        driver.close();
    }
}

