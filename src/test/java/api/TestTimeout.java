package api;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;

/**
 * Created by gooornik on 11.09.2019.
 */
public class TestTimeout {

    CloseableHttpClient client;
    CloseableHttpResponse response;

    @BeforeMethod(timeOut = 3000)
    public void setup(){
        client = HttpClientBuilder.create().build();
    }

    @Test(timeOut = 2000) //time in ms
    public void testIsTooSlow() throws IOException{

        //Act
        response = client.execute(new HttpGet("https://api.github.com/"));

        //Assert
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
    }

    @AfterMethod
    public void teardown() throws IOException {
        client.close();
        response.close();
    }
}
