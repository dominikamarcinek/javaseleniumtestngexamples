package api;

import app.CommonApiDataProviders;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.NotFoundMessage;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by gooornik on 11.09.2019.
 */

public class ApiTestWithDataProviders {

    protected CloseableHttpClient client;
    protected CloseableHttpResponse response;

    protected String baseEndpoint = "https://api.github.com/";

    @BeforeMethod
    public void buildClient(){
        // Arrange
        client = HttpClientBuilder.create().build();
    }

    @Test(dataProvider = "validGithubEndpoints",
            dataProviderClass = CommonApiDataProviders.class,
            description = "User, followers, notifications endpoints return 401 unauthorized status code")
    public void endpointsReturns401(String endpoint) throws IOException {
        // Act
        response = client.execute(new HttpGet(baseEndpoint + endpoint));
        int actualStatusCode = response.getStatusLine().getStatusCode();

        // Assert
        Assert.assertEquals(actualStatusCode, HttpStatus.SC_UNAUTHORIZED);
    }


    @Test(dataProvider = "differentStatuses",
            dataProviderClass = CommonApiDataProviders.class)
    public void eventsEndpointReturnsStatus(String status) throws IOException {
        // Act
        response = client.execute(new HttpGet(baseEndpoint + "events"));
        int actualStatusCode = response.getStatusLine().getStatusCode();
        System.out.println("Status received: " + actualStatusCode);
        // Assert
        Assert.assertNotEquals(actualStatusCode, status);
    }


    @Test
    public void userEndpointReturns401() throws IOException {
        // Act
        response = client.execute(new HttpGet("https://api.github.com/user"));
        int actualStatusCode = response.getStatusLine().getStatusCode();

        // Assert
        Assert.assertEquals(actualStatusCode, HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    public void issuesEndpointReturns404() throws IOException {
        // Act
        response = client.execute(new HttpGet("https://api.github.com/issues"));
        int actualStatusCode = response.getStatusLine().getStatusCode();

        // Assert
        Assert.assertEquals(actualStatusCode, HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void authorizationEndpointFieldCheck() throws IOException {
        // Act
        response = client.execute(new HttpGet("https://api.github.com/authorizations"));

        String jsonString = EntityUtils.toString(response.getEntity());
        ObjectMapper mapper = new ObjectMapper();
        NotFoundMessage notFoundMessage = mapper.readValue(jsonString, NotFoundMessage.class);

        System.out.println(notFoundMessage.toString());

        // Assert
        Assert.assertEquals("https://docs.github.com/rest", notFoundMessage.getDocumentationUrl());
    }



    @Test
    public void userFollowersEndpointReturns401() throws IOException {
        // Act
        response = client.execute(new HttpGet("https://api.github.com/user/followers"));
        int actualStatusCode = response.getStatusLine().getStatusCode();

        // Assert
        Assert.assertEquals(actualStatusCode, 401);
    }

    @Test
    public void notificationsFollowersEndpointReturns401() throws IOException {
        // Act
        response = client.execute(new HttpGet("https://api.github.com/notifications"));
        int actualStatusCode = response.getStatusLine().getStatusCode();

        // Assert
        Assert.assertEquals(actualStatusCode, 401);
    }

    @Test
    public void eventsEndpointReturns200() throws IOException {
        // Act
        response = client.execute(new HttpGet("https://api.github.com/events"));
        int actualStatusCode = response.getStatusLine().getStatusCode();
        String json = EntityUtils.toString(response.getEntity());
        System.out.println(json);

        // Assert
        Assert.assertEquals(actualStatusCode, HttpStatus.SC_OK);
    }

    @Test
    public void authorizationReturnsNotFound() throws IOException {
        // Act
        response = client.execute(new HttpGet("https://api.github.com/authorizations"));
        int actualStatusCode = response.getStatusLine().getStatusCode();
        String jsonString = EntityUtils.toString(response.getEntity());
        ObjectMapper mapper = new ObjectMapper();
        NotFoundMessage responseFromAuthorizations = mapper.readValue(jsonString, NotFoundMessage.class);
        System.out.println(responseFromAuthorizations.toString());

        // Assert
        Assert.assertEquals(actualStatusCode, HttpStatus.SC_NOT_FOUND);
        Assert.assertEquals(responseFromAuthorizations.getMessage(), "Not Found");
    }


    @AfterMethod
    public void cleanup() throws IOException {
        client.close();
        response.close();
    }
}
