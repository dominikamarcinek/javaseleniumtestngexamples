/*
 * Copyright (C) 2022 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 *
 * This software and other materials contain proprietary information
 * controlled by HERE and are protected by applicable copyright legislation.
 * Any use and utilization of this software and other materials and
 * disclosure to any third parties is conditional upon having a separate
 * agreement with HERE for the access, use, utilization or disclosure of this
 * software. In the absence of such agreement, the use of the software is not
 * allowed.
 */

package api;

import java.util.Scanner;

public class Main {

    public static void main (String args[]) {

        Scanner sc= new Scanner(System.in);    //System.in is a standard input stream
        System.out.print("Enter b:" );
        int b = sc.nextInt();
        System.out.print("Enter a: ");
        int a = sc.nextInt();
        System.out.print("Enter c: ");
        int c = sc.nextInt();

        double delta = Math.pow(b, 2) - 4*a*c;
        double sqrtDelta = Math.sqrt(delta);
        double element1 = (-b+ sqrtDelta)/(2*a);
        double element2 = (-b- sqrtDelta)/(2*a);


        System.out.printf("Delta=%s%n", delta);
        System.out.printf("element1=%s%n", element1);
        System.out.printf("element2=%s%n", element2);

    }
}
