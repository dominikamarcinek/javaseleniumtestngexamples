package api;

import baseclasses.WebServiceBaseClass;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.IOException;

import static org.apache.http.entity.ContentType.getOrDefault;

/**
 * Created by gooornik on 11.09.2019.
 */
public class WebServiceTestSoftAssert extends WebServiceBaseClass {

    @Test
    public void hardAssertStopsImmediately(){

        //Assert
        System.out.println("First assert");
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 404);

        System.out.println("Second assert");
        Assert.assertEquals(getOrDefault(response.getEntity()).getMimeType(), "application/xml");

        System.out.println("Third assert");
        Assert.assertEquals(getOrDefault(response.getEntity()).getCharset().toString(), "UTF-8");
    }

    @Test
    public void softAssertContinuesToTheEnd(){

        SoftAssert sa = new SoftAssert();

        //Assert
        System.out.println("First assert");
        sa.assertEquals(response.getStatusLine().getStatusCode(), 404);

        System.out.println("Second assert");
        sa.assertEquals(getOrDefault(response.getEntity()).getMimeType(), "application/xml");

        System.out.println("Third assert");
        sa.assertEquals(getOrDefault(response.getEntity()).getCharset().toString(), "UTF-8");

        sa.assertAll();
    }

    //Hard assert split into 3 different tests
    @Test
    public void statusIs200(){
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
    }

    @Test
    public void typeIsJson(){
        Assert.assertEquals(getOrDefault(response.getEntity()).getMimeType(), "application/json");

    }

    @Test
    public void charSetIsUtf8(){
        Assert.assertEquals(getOrDefault(response.getEntity()).getCharset().toString(), "UTF-8");
    }
}
