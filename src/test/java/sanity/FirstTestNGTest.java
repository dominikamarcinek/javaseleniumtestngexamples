package sanity;

import app.TestPriority;
import app.UserManager;
import app.DuplicateUserException;
import baseclasses.SanityTestBaseClass;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

/**
 * Created by gooornik on 11.09.2019.
 */

//@Test(dependsOnGroups = "sanity")
public class FirstTestNGTest extends SanityTestBaseClass {

    UserManager um;

    @BeforeMethod
    public void localSetup(Method testMethod){
        String desc = testMethod.getAnnotation(Test.class).description();
        System.out.println("Starting method: " + testMethod.getName() +
                " with desc: " + desc);

        um = new UserManager();
    }

    @Test(invocationCount = 2, priority = TestPriority.HIGH, description = "Verify that addUser method returns true when successful")
    public void successfulAddUserReturnsTrue() throws DuplicateUserException {
        //Arrange


        //Act
        boolean result = um.addUser("lukasz@gmail.com");

        //Assert
        Assert.assertFalse(result);
        //Assert.assertEquals(result, true);
    }

    @Test(description = "Verify that getUser method retrieves the correct existing user")
    public void getExistingUserReturnsExistingSavedUser() throws DuplicateUserException {
        //Arrange

        um.addUser("lukasz@gmail.com");

        //Act
        String user = um.getUser("lukasz@gmail.com");

        //Assert
        Assert.assertNotNull(user);
        Assert.assertEquals(user, "lukasz@gmail.com");
    }

    @Test(description = "Verify that getUser method returns null if the user does not exist")
    public void getNonExistingUserReturnsNull(){
        //Arrange

        //Act
        String nonExistingUser = um.getUser("lukasz@gmail.com");

        //Assert
        Assert.assertNull(nonExistingUser, "Method should return null if it doesn't find a user");
    }

    @Test(expectedExceptions = DuplicateUserException.class,
            expectedExceptionsMessageRegExp = ".*already exists")
    public void addDuplicateThrowsException() throws DuplicateUserException {
        //Act
        um.addUser("sameUser@gmail.com");
        um.addUser("sameUser@gmail.com");
    }

}
