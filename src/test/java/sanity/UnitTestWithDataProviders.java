package sanity;

import app.DuplicateUserException;
import app.UserManager;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by gooornik on 11.09.2019.
 */
public class UnitTestWithDataProviders{

    UserManager um;

    @DataProvider
    protected Object[][] invalidEmailProvider () {
        return new Object[][]{
                {""},
                {"lukaszgmail.com"},
                {"lukasz@gmailcom"},
        };
    }

    @BeforeMethod
    public void setup(){
        //Arrange
        um = new UserManager();
    }

    @Test(dataProvider = "invalidEmailProvider", expectedExceptions = IllegalArgumentException.class)
    public void emptyUserThrowsException(String invalidEmail) throws DuplicateUserException{
        //Act
        boolean result = um.addUser(invalidEmail);

        //Assert
        Assert.assertTrue(result);
    }



}
