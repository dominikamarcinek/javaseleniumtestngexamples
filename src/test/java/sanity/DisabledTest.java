package sanity;

import baseclasses.SanityTestBaseClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

/**
 * Created by gooornik on 11.09.2019.
 */

@Ignore
public class DisabledTest extends SanityTestBaseClass {

    @Test
    public void unstableTest1(){
        System.out.println("UnstableTest1");
    }

    @Test
    public void unstableTest2(){
        System.out.println("UnstableTest2");
    }
}
