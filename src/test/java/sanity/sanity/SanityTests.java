package sanity.sanity;

import app.UserManager;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by gooornik on 11.09.2019.
 */
@Test(groups = "sanity")
public class SanityTests {

    public void sanityStorageGetsCreatedAndIsEmpty(){
        UserManager um = new UserManager();
        Assert.assertTrue(um.getAllUsers().isEmpty());
    }

    public void sanityTest2(){
        System.out.println("Sanity test2");
    }
}
