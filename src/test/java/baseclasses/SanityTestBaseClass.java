package baseclasses;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

/**
 * Created by gooornik on 11.09.2019.
 */
public class SanityTestBaseClass {

    @BeforeSuite
    public void globalSetup(){
        System.out.println("Global setup");
    }

    @BeforeMethod
    public void globalBeforeMethodSetup(){
        System.out.println("This is global beforeMethod");
    }

    //don't use inheritance with dataProviders
    @DataProvider
    protected Object[][] invalidEmailProvider () {
        return new Object[][]{
                {""},
                {"lukaszgmail.com"},
                {"lukasz@gmailcom"}
        };
    }
}
